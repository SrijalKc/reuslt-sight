from django.urls import path, include
from rest_framework import routers
from . import views
from .views import (
    PostListView,
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,

)

# router = routers.DefaultRouter()
# router.register('postapi', views.PostSerializeView)

urlpatterns = [
    path('', PostListView.as_view(), name='Basics-home'),
    path('user/<str:full_name>/', UserPostListView.as_view(), name='user-posts'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('about/', views.about, name='Basics-about'),
    path('chart/', views.display_chart, name='Basics-chart'),
    path('chart_general/', views.display_chart_general, name='Basics-chart-general'),
    path('chart_line/', views.display_chart_line, name='Basics-chart-line'),
    path('results/', views.result, name='Basics-results'),
    path('result/pdf/<str:term>/', views.generate_pdf, name='pdf'),
    path('students-results/', views.students_result, name='Basics-student-results'),
    path('post/new/ajax/', views.load_subjects, name='ajax-load-subject'),
    # path('api/', include(router.urls))
]
