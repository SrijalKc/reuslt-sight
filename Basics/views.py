from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Result, Post, Marks
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import get_user_model
from users.decorators import teacher_required
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .forms import PostForm
from users.models import Subject, UserTeacher, UserStudent, Semester, Course, ExamTitle
from .serializers import PostSerializer
from .utils import render_to_pdf

User = get_user_model()


# Create your views here.

def about(request):
    return render(request, 'Basics/about.html', {'title': 'About'})


class PostListView(ListView):
    model = Post
    template_name = 'Basics/home.html'
    context_object_name = "post"
    ordering = ['-date_posted']
    paginate_by = 5



class UserPostListView(ListView):
    model = Post
    template_name = 'Basics/user_posts.html'
    context_object_name = 'post'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User,full_name=self.kwargs.get('full_name'))  # that full_name is from our custom User Model
        teacher = user.userteacher.id
        return Post.objects.filter(teacher=user).order_by('-date_posted')


class PostDetailView(DetailView):
    model = Post
    template_name = 'Basics/post_detail.html'


@method_decorator([teacher_required], name='dispatch')
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    form_class = PostForm
    template_name = "Basics/post_form.html"

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = PostForm(request.POST, user=request.user)
            if form.is_valid():
                form.save(commit=False)
                user = request.user.id
                batch_id = request.POST.get('batch')
                subject_id = request.POST.get('subject')
                student_ids = request.POST.getlist('students[]')
                mark_ids = request.POST.getlist('marks[]')
                title_id = request.POST.get('title')
                title = ExamTitle.objects.get(id=title_id).name
                description = request.POST.get('description')
                posts = []
                count = 0
                post = Post(title_id=title_id, description=description, teacher_id=user)
                post.save()
                post_object = Post.objects.last()
                print("+++")
                print(post_object)
                for i in range(len(student_ids)):
                    marks = Marks(marks_obtained=mark_ids[i], student_id=student_ids[i], subject_id=subject_id,
                                  post_id=Post.objects.last().id)
                    marks.save()
                form.instance.teacher = self.request.user
                messages.success(request, f'Your post has been posted!')
                return redirect('Basics-home')

        else:
            form = PostForm()
        return render(request, self.template_name, {'form': form})

    def form_valid(self, form):
        form.instance.teacher = self.request.user
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(PostCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


def students_result(request):
    user = request.user
    user_id = user.id
    user_fullname = user.get_full_name()
    student_id = UserStudent.objects.get(user_id=user_id).id
    semester_qs = UserStudent.objects.filter(user_id=user_id).values('semester__name')
    course_qs = UserStudent.objects.filter(user_id=user_id).values('course__name')
    semester = str(semester_qs[0]['semester__name'])
    course = str(course_qs[0]['course__name'])
    subjects = list(Subject.objects.filter(course__name=course, semester__name=semester).values_list('name', flat=True))
    subjects_id = list(
        Subject.objects.filter(course__name=course, semester__name=semester).values_list('id', flat=True))
    marks_obtained = []
    full_marks = []
    pass_marks = []
    title_id = []
    title_name = []
    post = [list(Marks.objects.filter(student_id=student_id).values_list('post', flat=True))]
    for p in post:
        for n in p:
            title_id.append(list(Post.objects.filter(id=n).values_list('title_id', flat=True)))

    for t in title_id:
        for n in t:
            title_name.append(list(ExamTitle.objects.filter(id=n).values_list('name', flat=True)))

    PP = list(Post.objects.filter(title_id=2).values_list('title__name', flat=True))

    for sub in subjects_id:
        full_marks.append(list(Subject.objects.filter(id=sub).values('full_marks').values_list(
            'full_marks', flat=True)))
        pass_marks.append(list(Subject.objects.filter(id=sub).values('pass_marks').values_list(
            'pass_marks', flat=True)))
        marks_obtained.append(list(Marks.objects.filter(student_id=student_id, subject_id=sub).values_list(
            'marks_obtained', flat=True)))


    for pp in post:
        for p in pp:
            if marks_obtained < pass_marks:
                remarks='Failed'
                break
            else:
                remarks='Passes'


    all_result = {}
    all_marks = Marks.objects.filter(student_id=student_id)
    all_titles = {}
    all_semester = []
    total = 0
    for marks in all_marks:
        semester_name = marks.subject.semester.name
        post_title = marks.post.title.name
        if semester_name not in all_semester:
            all_semester.append(semester_name)
            all_result[semester_name] = {}
            all_titles[semester_name] = []
        if post_title not in all_titles[semester_name]:
            all_titles[semester_name].append(post_title)
            all_result[semester_name][post_title] = {}
            all_result[semester_name][post_title]['marks'] = []
            all_result[semester_name][post_title]['total'] = {}
            all_result[semester_name][post_title]['total']['obtained'] = 0
            all_result[semester_name][post_title]['total']['marks'] = 0
            all_result[semester_name][post_title]['total']['pass_marks'] = 0
            all_result[semester_name][post_title]['total']['percentage'] = 0
            all_result[semester_name][post_title]['total']['remarks'] = ''
        all_result[semester_name][post_title]['marks'].append(
            [marks.subject.name, marks.subject.full_marks, marks.subject.pass_marks, marks.marks_obtained, ])
        all_result[semester_name][post_title]['total']['obtained'] += marks.marks_obtained
        all_result[semester_name][post_title]['total']['marks'] += marks.subject.full_marks
        all_result[semester_name][post_title]['total']['pass_marks'] = marks.subject.pass_marks
        all_result[semester_name][post_title]['total']['percentage'] = (all_result[semester_name][post_title]['total'][
            'obtained']) / (all_result[semester_name][post_title]['total']['marks']) * 100

    context = {
        'full_name': user_fullname,
        'semester': semester,
        'course': course,
        'result': all_result,
    }
    print("RESULT")
    print(all_result)
    return render(request, "Basics/student_results.html", context)


def result(request):
    user = request.user
    user_id = user.id
    teacher_id = UserTeacher.objects.get(user_id=user_id).id
    courses = Course.objects.filter(userteacher=teacher_id).values_list('id', flat=True)
    courses_name = Course.objects.filter(userteacher=teacher_id)

    semesters = UserTeacher.objects.filter(id=teacher_id).values_list('semester', flat=True)
    semesters_id = UserTeacher.objects.filter(id=teacher_id)


    all_result = {}

    all_students_temp = []
    all_students = []
    all_marks = []
    every_students = {}
    all_titles = {}
    all_semester = []

    results = {}
    teacher_marks = Marks.objects.filter(post__teacher=request.user)

    for mark in teacher_marks:
        course = mark.subject.course.get()
        semester = mark.subject.semester
        subject = mark.subject
        student = mark.student.user
        batch = str(mark.student.batch.date)
        print(batch)
        term = mark.post.title
        if course.name not in results.keys():
            results[course.name] = {}
        if batch not in results[course.name].keys():
            results[course.name][batch] = {}
        if semester.name not in results[course.name][batch].keys():
            results[course.name][batch][semester.name] = {}
        if subject.name not in results[course.name][batch][semester.name].keys():
            results[course.name][batch][semester.name][subject.name] = {}
        if student.full_name not in results[course.name][batch][semester.name][subject.name].keys():
            results[course.name][batch][semester.name][subject.name][student.full_name] = {}
        if term.name not in results[course.name][batch][semester.name][subject.name][student.full_name].keys():
            results[course.name][batch][semester.name][subject.name][student.full_name][term.name] = {
                'full_marks': subject.full_marks,
                'pass_marks': subject.pass_marks,
                'obtained_marks': mark.marks_obtained
            }

    print(results)

    context = {
        'results': results,
        'a': '1',
        'b': '2'
    }

    return render(request, 'Basics/teacher_results.html', context)


def display_chart(request):
    user = request.user
    user_id = user.id
    user_fullname = user.get_full_name()
    student_id = UserStudent.objects.get(user_id=user_id).id
    all_result = {
        'IT': 67,
        'C': 99
    }
    all_result = {}
    all_marks = Marks.objects.filter(student_id=student_id)
    all_titles = {}
    all_semester = []
    total = 0
    for marks in all_marks:
        semester_name = marks.subject.semester.name
        post_title = marks.post.title.name
        if semester_name not in all_semester:
            all_semester.append(semester_name)
            all_result[semester_name] = {}
            all_titles[semester_name] = []
        if post_title not in all_titles[semester_name]:
            all_titles[semester_name].append(post_title)
            all_result[semester_name][post_title] = {}
            all_result[semester_name][post_title]['marks'] = []
            all_result[semester_name][post_title]['total'] = {}
            all_result[semester_name][post_title]['total']['obtained'] = 0
            all_result[semester_name][post_title]['total']['marks'] = 0
            all_result[semester_name][post_title]['total']['pass_marks'] = 0
            all_result[semester_name][post_title]['total']['percentage'] = 0
            all_result[semester_name][post_title]['total']['remarks'] = ''
        all_result[semester_name][post_title]['marks'].append(
            [marks.subject.name, marks.subject.full_marks, marks.subject.pass_marks, marks.marks_obtained, ])
        all_result[semester_name][post_title]['total']['obtained'] += marks.marks_obtained
        all_result[semester_name][post_title]['total']['marks'] += marks.subject.full_marks
        all_result[semester_name][post_title]['total']['pass_marks'] += marks.subject.pass_marks
        all_result[semester_name][post_title]['total']['percentage'] = (all_result[semester_name][post_title]['total'][
            'obtained']) / (all_result[semester_name][post_title]['total']['marks']) * 100

    context = {
        'result': all_result
    }

    return render(request, 'Basics/display_chart.html', context)


def display_chart_general(request):
    user = request.user
    user_id = user.id
    user_fullname = user.get_full_name()
    student_id = UserStudent.objects.get(user_id=user_id).id
    all_result = {
        'IT': 67,
        'C': 99
    }
    all_result = {}
    all_marks = Marks.objects.filter(student_id=student_id)
    all_titles = {}
    all_semester = []
    total = 0
    for marks in all_marks:
        semester_name = marks.subject.semester.name
        post_title = marks.post.title.name
        if semester_name not in all_semester:
            all_semester.append(semester_name)
            all_result[semester_name] = {}
            all_titles[semester_name] = []
        if post_title not in all_titles[semester_name]:
            all_titles[semester_name].append(post_title)
            all_result[semester_name][post_title] = {}
            all_result[semester_name][post_title]['marks'] = []
            all_result[semester_name][post_title]['total'] = {}
            all_result[semester_name][post_title]['total']['obtained'] = 0
            all_result[semester_name][post_title]['total']['marks'] = 0
            all_result[semester_name][post_title]['total']['pass_marks'] = 0
            all_result[semester_name][post_title]['total']['percentage'] = 0
            all_result[semester_name][post_title]['total']['remarks'] = ''
        all_result[semester_name][post_title]['marks'].append(
            [marks.subject.name, marks.subject.full_marks, marks.subject.pass_marks, marks.marks_obtained, ])
        all_result[semester_name][post_title]['total']['obtained'] += marks.marks_obtained
        all_result[semester_name][post_title]['total']['marks'] += marks.subject.full_marks
        all_result[semester_name][post_title]['total']['pass_marks'] += marks.subject.pass_marks
        all_result[semester_name][post_title]['total']['percentage'] = (all_result[semester_name][post_title]['total'][
            'obtained']) / (all_result[semester_name][post_title]['total']['marks']) * 100

    context = {
        'result': all_result
    }

    return render(request, 'Basics/dsplay_chart_general.html', context)


def display_chart_line(request):
    user = request.user
    user_id = user.id
    user_fullname = user.get_full_name()
    student_id = UserStudent.objects.get(user_id=user_id).id

    all_result = {}
    all_marks = Marks.objects.filter(student_id=student_id)
    all_titles = {}
    all_semester = []
    total = 0
    for marks in all_marks:
        semester_name = marks.subject.semester.name
        post_title = marks.post.title.name
        if semester_name not in all_semester:
            all_semester.append(semester_name)
            all_result[semester_name] = {}
            all_titles[semester_name] = []
        if post_title not in all_titles[semester_name]:
            all_titles[semester_name].append(post_title)
            all_result[semester_name][post_title] = {}
            all_result[semester_name][post_title]['marks'] = []
            all_result[semester_name][post_title]['total'] = {}
            all_result[semester_name][post_title]['total']['obtained'] = 0
            all_result[semester_name][post_title]['total']['marks'] = 0
            all_result[semester_name][post_title]['total']['pass_marks'] = 0
            all_result[semester_name][post_title]['total']['percentage'] = 0
            all_result[semester_name][post_title]['total']['remarks'] = ''
        all_result[semester_name][post_title]['marks'].append(
            [marks.subject.name, marks.subject.full_marks, marks.subject.pass_marks, marks.marks_obtained, ])
        all_result[semester_name][post_title]['total']['obtained'] += marks.marks_obtained
        all_result[semester_name][post_title]['total']['marks'] += marks.subject.full_marks
        all_result[semester_name][post_title]['total']['pass_marks'] += marks.subject.pass_marks
        all_result[semester_name][post_title]['total']['percentage'] = (all_result[semester_name][post_title]['total'][
            'obtained']) / (all_result[semester_name][post_title]['total']['marks']) * 100

    context = {
        'result': all_result
    }

    return render(request, 'Basics/display_chart_line_chart.html', context)


def generate_pdf(request, term):
    user = request.user
    user_id = user.id
    user_fullname = user.get_full_name()
    semester_qs = UserStudent.objects.filter(user_id=user_id).values('semester__name')
    course_qs = UserStudent.objects.filter(user_id=user_id).values('course__name')
    course = str(course_qs[0]['course__name'])
    semester = str(semester_qs[0]['semester__name'])

    student_id = UserStudent.objects.get(user_id=user_id).id
    term_id = ExamTitle.objects.get(name=term).id

    all_result = {}
    all_marks = Marks.objects.filter(post__title=term_id, student_id=student_id)
    all_titles = {}
    all_semester = []
    total = 0
    for marks in all_marks:
        semester_name = marks.subject.semester.name
        post_title = marks.post.title.name
        if semester_name not in all_semester:
            all_semester.append(semester_name)
            all_result[semester_name] = {}
            all_titles[semester_name] = []
        if post_title not in all_titles[semester_name]:
            all_titles[semester_name].append(post_title)
            all_result[semester_name][post_title] = {}
            all_result[semester_name][post_title]['marks'] = []
            all_result[semester_name][post_title]['total'] = {}
            all_result[semester_name][post_title]['total']['obtained'] = 0
            all_result[semester_name][post_title]['total']['marks'] = 0
            all_result[semester_name][post_title]['total']['pass_marks'] = 0
            all_result[semester_name][post_title]['total']['percentage'] = 0
            all_result[semester_name][post_title]['total']['remarks'] = ''
        all_result[semester_name][post_title]['marks'].append(
            [marks.subject.name, marks.subject.full_marks, marks.subject.pass_marks, marks.marks_obtained, ])
        all_result[semester_name][post_title]['total']['obtained'] += marks.marks_obtained
        all_result[semester_name][post_title]['total']['marks'] += marks.subject.full_marks
        all_result[semester_name][post_title]['total']['pass_marks'] += marks.subject.pass_marks
        all_result[semester_name][post_title]['total']['percentage'] = (all_result[semester_name][post_title]['total'][
            'obtained']) / (all_result[semester_name][post_title]['total']['marks']) * 100


    print(all_result)
    template = get_template('Basics/pdfFile.html')
    context = {
        'full_name': user_fullname,
        'semester': semester,
        'course': course,
        'result': all_result,
    }
    html = template.render(context)
    pdf = render_to_pdf('Basics/pdfFile.html', context)
    return HttpResponse(pdf, content_type='application/pdf')



# @method_decorator([teacher_required], name='dispatch')
# class ResultCreateView(LoginRequiredMixin, CreateView):
#     model = Marks
#     form_class = ResultForm
#     template_name = "Basics/result_form.html"
#
#     def post(self, request, *args, **kwargs):
#         # semester = request.session.get('semester')
#         # course = request.session.get('course')
#         # subject = request.session.get('subject')
#         user = request.user.id
#         semester_taught = Semester.objects.filter(userteacher=user)
#         subject_taught = Subject.objects.filter(teacher=user)
#
#         # print("THIS IS USER")
#         # print(semester_taught)
#         semester = request.POST.get('semester')
#         course = request.POST.get('course')
#         subject = request.POST.get('subject')
#         students = UserStudent.objects.filter(course_id=course).filter(semester_id=semester)
#         # print("IIIII")
#         # print(students)
#         semester = Semester.objects.get(id=semester).name
#         course = Course.objects.get(id=course).name
#         subject = Subject.objects.get(id=subject).name
#         my_dict = {'semester': semester,
#                    'subject': subject,
#                    'course': course}
#         # def get_initial(self):
#         #     return {'semester': semester, 'course': course}
#         # my_dict = {'semester': semester,
#         #            'course': course,yufujftjjuuuddfsdf
#         #            'subject': subject}
#         if request.method == 'POST':
#             form = ResultForm(request.POST)
#             if form.is_valid():
#                 new_form = form.save(commit=False)
#                 subject = form.cleaned_data.get('subject')
#                 semester = form.cleaned_data.get('semester')
#                 course = form.cleaned_data.get('course')
#                 subject = Subject.objects.get(name=subject).id
#                 # print("OOOOOO")
#                 # print(subject)
#                 student = request.POST.getlist('student')
#                 marks = request.POST.getlist('marks_obtained')
#                 count = len(students)
#                 student = student[:count - 1]
#                 marks = marks[:count - 1]
#                 a = zip_longest(student, marks)
#                 for s, m in a:
#                     # user_id = UserStudent.objects.get(id=s).user_id
#                     # name = User.objects.get(id=user_id).full_name
#                     Marks.objects.create(marks_obtained=m, student_id=s, subject_id=subject)
#                     # print("this is student")
#                     # print(s)
#                     # print("this is marks")
#                     # print(m)
#                 new_form.save()
#                 messages.success(request, f'Your post has been posted!')
#                 return redirect('report-generate')
#         else:
#             form = ResultForm()
#         return render(request, self.template_name, {'form': form, 'my_dict': my_dict, 'students': students})
#
#     def get_form_kwargs(self):
#         kwargs = super(ResultCreateView, self).get_form_kwargs()
#         kwargs.update({'user': self.request.user})
#         return kwargs


@method_decorator([teacher_required], name='dispatch')
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['student', 'marks_obtained']

    def form_valid(self, form):
        form.instance.teacher = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user:
            return True
        else:
            return False


@method_decorator([teacher_required], name='dispatch')
class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.teacher:
            return True
        else:
            return False


@csrf_exempt
def load_subjects(request):
    if request.method == 'POST':
        semester_id = request.POST.get('semester')
        course_id = request.POST.get('course')
        batch_id = request.POST.get('batch')
        print(request.POST)
        print(batch_id)
        print(course_id)
        teacher_id = UserTeacher.objects.get(user_id=request.user.id).pk

        result = []
        students = {}
        for c_id in course_id:
            for b_id in batch_id:
                for s_id in semester_id:
                    students = load_students(c_id, s_id, b_id)
                    subjects = Subject.objects.filter(course=c_id).filter(semester_id=s_id).filter(teacher=teacher_id)
                    for subject in subjects:
                        result.append({'id': subject.id, 'name': subject.name})


        # return JsonResponse(json.dumps(result), safe=False)
        return render(request, "Basics/dropdown_subject.html", {'subjects': result, 'students': students})


def load_students(course_id, semester_id, batch_id):
    students = UserStudent.objects.filter(course_id=course_id, semester_id=semester_id, batch_id=batch_id).values('user__full_name', 'id')
    return students


# class PostSerializeView(viewsets.ModelViewSet):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer


def get_data(request, *args, **kwargs):
    data = {
        'name': "srijal",
        'marks': 20
    }
    return JsonResponse(data)


class ChartData(APIView):
    def get(self, request, format=None):
        students_Count = len(list(Post.objects.all().values('student').values_list('student', flat=True)))
        students = Post.objects.all().values('student').values_list('student', flat=True)[:(students_Count / 2)]
        student_names = []
        marks_obtaineds = []
        for s in students:
            student_name = UserStudent.objects.get(id=s).user.full_name
            student_names.append(student_name)
            marks_obtained = Post.objects.filter(student=s).values('marks_obtained').values_list('marks_obtained',
                                                                                                 flat=True)
            marks_obtaineds.append(marks_obtained)
        data = {
            'student': student_names,
            'marks_obtained': marks_obtaineds
        }
        return Response(data)
