from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.urls import reverse
from users.models import UserStudent, Subject, ExamTitle

User = get_user_model()


# Create your models here.
class Post(models.Model):
    title = models.ForeignKey(ExamTitle, on_delete=models.PROTECT, blank=False)
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    description = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title.name

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})


class Marks(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    student = models.ForeignKey(UserStudent, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    marks_obtained = models.IntegerField()

    def __str__(self):
        return self.subject.name


class Result(models.Model):
    title = models.CharField(max_length=30)
    student = models.OneToOneField(UserStudent, on_delete=models.CASCADE)
    remarks = models.CharField(max_length=10, default='Pass')
    division = models.FloatField(max_length=10, default=30.0)
    grand_total = models.IntegerField(default=0)

    def __str__(self):
        return self.title



