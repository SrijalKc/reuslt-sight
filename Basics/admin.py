from django.contrib import admin
from .models import Result, Post, Marks

# Register your models here.
admin.site.register(Result)
admin.site.register(Post)
admin.site.register(Marks)
