from django import forms
from django.contrib.auth import get_user_model
from .models import Post, Result
from users.models import Course, Semester, Subject, UserTeacher, UserStudent, Batch


class PostForm(forms.ModelForm):
    course = forms.ModelChoiceField(
        queryset=Course.objects.all()
    )
    semester = forms.ModelChoiceField(
        queryset=Semester.objects.all()
    )
    subject = forms.ModelChoiceField(
        queryset=Subject.objects.all()
    )

    batch = forms.ModelChoiceField(
        queryset=Batch.objects.all()
    )

    class Meta:
        model = Post
        fields = ['title', 'description', 'course', 'batch', 'semester', 'subject', ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        teacher_id = self.user.userteacher.id
        self.fields['course'].queryset = Course.objects.filter(userteacher=teacher_id)
        self.fields['semester'].queryset = Semester.objects.filter(userteacher=teacher_id)
        self.fields['batch'].queryset = Batch.objects.filter()


# class ResultForm(forms.ModelForm):
#     course = forms.ModelChoiceField(
#         queryset=Course.objects.all()
#     )
#     semester = forms.ModelChoiceField(
#         queryset=Semester.objects.all()
#     )
#     subject = forms.ModelMultipleChoiceField(
#         queryset=Subject.objects.all(), widget=forms.CheckboxSelectMultiple
#     )
#
#     class Meta:
#         model = Marks
#         fields = ['semester', 'course', 'subject', 'student', 'marks_obtained', ]
#
#     def __init__(self, *args, **kwargs):
#         self.user = kwargs.pop('user')
#         super().__init__(*args, **kwargs)
#         teacher_id = self.user.userteacher.id
#         courses = Course.objects.filter(userteacher=teacher_id)
#         semesters = Semester.objects.filter(userteacher=teacher_id)
#         print(teacher_id)
#         self.fields['course'].queryset = courses
#         self.fields['semester'].queryset = semesters
        # self.fields['subject'].queryset = Subject.objects.filter(teacher=teacher_id)
        # self.fields['student'].queryset = UserStudent.objects.filter(teacher_id=teacher_id))
        # counts = UserStudent.objects.filter(course_id=course).filter(semester_id=semester).count()

        # if my_list:
        #     self.fields['course'].queryset = my_list[1]
        #     self.fields['semester'].queryset = my_list[0]
        #     self.fields['subject'].queryset = my_list[2]

    # def clean(self):
    #     cleaned_data = super(ResultForm, self).clean()
    #     course = cleaned_data.get('course')
    #     semester = cleaned_data.get('semester')
    #     subject = cleaned_data.get('subject')
    #     print(course)
    #     print(subject)
    #     print(semester)


class ReportForm(forms.ModelForm):
    course = forms.ModelChoiceField(
        queryset=Course.objects.all()
    )
    semester = forms.ModelChoiceField(
        queryset=Semester.objects.all()
    )
    subject = forms.ModelChoiceField(
        queryset=Subject.objects.all()
    )

    class Meta:
        model = Result
        fields = ['course', 'semester', 'student', 'remarks', 'division', 'title', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        course = self.data.get('course')
        # print("Course_id" + str(course))
        semester = self.data.get('semester')
        # print("semester_id" + str(semester))
        subject = self.data.get('subject')
        # print("Subject_id" + str(subject))
        # self.fields['subject'].queryset = Subject.objects.filter(id=subject)
        self.fields['course'].queryset = Course.objects.filter(id=course)
        self.fields['semester'].queryset = Semester.objects.filter(id=semester)
        self.fields['student'].queryset = UserStudent.objects.filter(course_id=course).filter(semester_id=semester)


