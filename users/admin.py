from django.contrib import admin
from .models import Profile, Course, Semester, UserTeacher, UserStudent, Subject, ExamTitle, Batch
from django.contrib.auth import get_user_model
from .forms import (
    UserAdminCreationForm,
    UserAdminChangeForm
)
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

User = get_user_model()


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'admin')
    list_filter = ('admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Full Name', {'fields': ('full_name',)}),
        ('Permissions', {'fields': ('admin', 'staff', 'active')}),
        ('Post', {'fields': ('student', 'teacher')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'full_name',)
    ordering = ('email',)
    filter_horizontal = ()


# Register your models here.
admin.site.register(Profile)
admin.site.register(User, UserAdmin)
admin.site.register(Course)
admin.site.register(UserStudent)
admin.site.register(UserTeacher)
admin.site.register(Semester)
admin.site.register(ExamTitle)
admin.site.register(Batch)




# @admin.register(Subject)
# class SubjectAdmin(admin.ModelAdmin):
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#
#         if db_field.name == 'semester':
#             kwargs["queryset"] = Semester.objects.filter(userteacher=a.id)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Subject)
