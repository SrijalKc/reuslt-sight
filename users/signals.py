from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from .models import Profile
from .models import UserStudent

User = get_user_model()


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    instance.profile.save()


# def  create_user_student(sender, **kwargs):
#     if kwargs['created']:
#         userStudent = UserStudent.objects.create(user=kwargs['instance'])


# post_save.connect(create_user_student, sender=User)


# @receiver(post_save, sender=User)
# def create_user_student(sender, instance, created,  **kwargs):
#     if created:
#         a = UserStudent.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_user_student(sender, instance, created, **kwargs):
#     instance.a.save()