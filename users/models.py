# from django.conf import settings
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager
)
from PIL import Image
from smart_selects.db_fields import ChainedForeignKey


# Create your models here.
class UserManager(BaseUserManager):
    def create_user(self, email, full_name, password=None, is_active=True, is_staff=False, is_admin=False,
                    is_teacher=False, is_student=False):
        if not email:
            return ValueError("Users must have an email address!!!")

        if not password:
            return ValueError('Users must have password!!!')

        user_obj = self.model(
            email=self.normalize_email(email),
            full_name=full_name,
        )
        user_obj.set_password(password)
        user_obj.active = is_active
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.teacher = is_teacher
        user_obj.student = is_student
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email, full_name, password=None):
        user = self.create_user(
            email=email,
            full_name=full_name,
            password=password,
            is_staff=True
        )
        return user

    def create_superuser(self, email, full_name, password=None):
        user = self.create_user(
            email=email,
            full_name=full_name,
            password=password,
            is_staff=True,
            is_admin=True
        )
        return user

    def create_student(self, email, full_name, password=None):
        user = self.create_user(
            email=email,
            full_name=full_name,
            password=password,
            is_student=True
        )
        return user

    def create_teacher(self, email, full_name, password=None):
        user = self.create_user(
            email=email,
            full_name=full_name,
            password=password,
            is_teacher=True
        )
        return user


class User(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True, default='abc@gmail.com')
    full_name = models.CharField(max_length=30, blank=True, null=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    teacher = models.BooleanField(default=False)
    student = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['full_name']

    def __str__(self):
        return self.email

    def get_full_name(self):
        if self.full_name:
            return self.full_name
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

    @property
    def is_student(self):
        return self.student

    @property
    def is_teacher(self):
        return self.teacher


class ExamTitle(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile-pics')

    def __str__(self):
        return f' {self.user.full_name}\'s Profile'

    def save(self, *args, **kwargs):
        super().save()
        img = Image.open(self.image.path)
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save()


class Course(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Semester(models.Model):
    name = models.CharField(max_length=255)

    # course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Batch(models.Model):
    name = models.CharField(max_length=255)
    date = models.IntegerField()

    def __str__(self):
        return str(self.date)


class UserTeacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    course = models.ManyToManyField(Course)
    semester = models.ManyToManyField(Semester)
    # batch = models.ForeignKey(Batch, on_delete=models.CASCADE, default=1)

    # subject = models.ForeignKey(Subject, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.user.email


class Subject(models.Model):
    name = models.CharField(max_length=255)
    full_marks = models.IntegerField()
    pass_marks = models.IntegerField()
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    course = models.ManyToManyField(Course)
    teacher = models.ManyToManyField(UserTeacher, blank=True, null=True)

    def __str__(self):
        return self.name


class UserStudent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='studentuser')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course')
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, default=1)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.full_name
