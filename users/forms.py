from django import forms
from django.contrib.auth import get_user_model
# from .models import User
from django.contrib.auth.forms import UserCreationForm, ReadOnlyPasswordHashField
from .models import Profile, Course, Semester, Subject, UserStudent, UserTeacher, Batch

# from itertools import zip_longest
# from django_select2.forms import ModelSelect2Widget
# from smart_selects.db_fields import ChainedForeignKey

User = get_user_model()


class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('full_name', 'email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('full_name', 'email', 'password', 'active', 'admin',)

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


# class UserRegisterForm(UserCreationForm):
#     class Meta:
#         model = User
#         fields = ['full_name', 'email', 'password1', 'password2']


class StudentRegisterForm(UserCreationForm):
    course = forms.ModelChoiceField(
        queryset=Course.objects.all(),
    )
    semester = forms.ModelChoiceField(
        queryset=Semester.objects.all()
    )
    batch = forms.ModelChoiceField(
        queryset=Batch.objects.all()
    )

    class Meta:
        model = User
        fields = ['full_name', 'email', 'password1', 'password2', 'course', 'semester', 'batch']

    # def clean(self):
    #     cleaned_data = super(StudentRegisterForm, self).clean()
    #     email = cleaned_data.get('email')
    #     emails = User.objects.filter(email=email)
    #     # e = emails.user.pk
    #     print(emails)

    def save(self, **kwargs):
        user = super().save(commit=False)  # COMPARE ABOVE
        user.student = True
        user.save()
        return user


class TeacherRegisterForm(UserCreationForm):
    course = forms.ModelMultipleChoiceField(
        queryset=Course.objects.all(), widget=forms.CheckboxSelectMultiple
    )
    semester = forms.ModelMultipleChoiceField(
        queryset=Semester.objects.all(), widget=forms.CheckboxSelectMultiple
    )
    subject = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(), widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = User
        fields = ['full_name', 'email', 'password1', 'password2', ]

    # def clean(self):
    #     cleaned_data = super(TeacherRegisterForm, self).clean()
    #     course = cleaned_data.get('course')
    #     semester = cleaned_data.get('semester')
    #     for c in course:
    #         course_id = Course.objects.get(name=c).pk
    #         courses.append(course_id)
    #     for s in semester:
    #         semester_id = Semester.objects.get(name=s).pk

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['subject'].queryset = Subject.objects.none()

    # semester_ids= self.data.getlist('semester')
    # course_ids = self.data.getlist('course')
    # a = zip_longest(semester_ids, course_ids)
    # courses = []
    # result = []
    # for c_id in course_ids:
    #     # print(c_id)
    #     for s_id in semester_ids:
    #         subjects  = Subject.objects.filter(course=c_id ).filter(semester_id=s_id)
    #         for subject in subjects:
    #             result.append([subject.id, subject.name])

    # print(result)
    # courses.append(course)
    # print(courses)
    # for s_id in semester_ids:
    #     subjects = Subject.objects.filter(semester_id=s_id)
    # print(subjects)

    # for s_id, c_id in a:
    # self.fields['subject'].queryset = (Subject.objects.filter(semester_id=s_id) | Subject.objects.filter(course__subject__course=c_id ))
    #     print(s_id, c_id)
    # self.fields['subject'].queryset = Subject.objects.none()
    # if 'semester' in self.data:
    #     try:
    #         semester_id = int(self.data.get('semester'))
    #         self.fields['subject'].queryset = Subject.objects.filter(semester_id=semester_id)
    #     except (ValueError, TypeError):
    #         pass  # invalid input from the client; ignore and fallback to empty City queryset
    # elif self.instance.pk:
    #     self.fields['subject'].queryset = self.instance.semester.subject_set

    def save(self, **kwargs):
        user = super().save(commit=False)  # COMPARE ABOVE
        user.teacher = True
        user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    full_name = forms.CharField(max_length=20)

    class Meta:
        model = User
        fields = ['full_name', 'email',]


class StudentUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    full_name = forms.CharField(max_length=20)

    class Meta:
        model = UserStudent
        fields = ['full_name', 'email', 'semester', 'course']


class TeacherUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    full_name = forms.CharField(max_length=20)

    class Meta:
        model = UserTeacher
        fields = ['full_name', 'email', 'semester', 'course']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']

        # this is test
