from django.urls import path
from . import views
from .views import StudentSignUpView, TeacherSignUpView

urlpatterns = [
    path('', views.register, name='users-register'),
    path('student/', StudentSignUpView.as_view(), name='users-student'),
    path('teacher/', TeacherSignUpView.as_view(), name='users-teacher'),
    path('teacher/ajax', views.load_subjects, name='ajax-load-subjects')

]
