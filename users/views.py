from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from .forms import UserUpdateForm, ProfileUpdateForm, StudentRegisterForm, TeacherRegisterForm, StudentUpdateForm, TeacherUpdateForm
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from django.contrib.auth import get_user_model, login
from .models import UserStudent, Course, Semester, UserTeacher, Subject, Batch
from itertools import zip_longest
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import JsonResponse

User = get_user_model()


# Create your views here.


def register(request):
    return render(request, 'users/register_as.html')


class StudentSignUpView(CreateView):
    model = User
    form_class = StudentRegisterForm  # add this when u have Custom Forms. Also no need to include "fields" attribute after including "form_class"
    template_name = 'users/register_as_student.html'

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = StudentRegisterForm(request.POST)
            if form.is_valid():
                new_form = form.save(commit=False)
                username = form.cleaned_data.get('full_name')
                course = form.cleaned_data.get('course')
                semester = form.cleaned_data.get('semester')
                batch = form.cleaned_data.get('batch')
                user_id = User.objects.get(full_name=username).pk
                course_id = Course.objects.get(name=course).pk
                semester_id = Semester.objects.get(name=semester).pk
                UserStudent.objects.create(user_id=user_id, batch_id=batch.id, course_id=course_id, semester_id=semester_id, )
                new_form.save()
                messages.success(request, f'Account Created. {username} can now login as Student.')
                return redirect('users-login')
        else:
            form = StudentRegisterForm()

        return render(request, self.template_name, {'form': form})



class TeacherSignUpView(CreateView):
    model = User
    form_class = TeacherRegisterForm
    template_name = 'users/register_as_teacher.html'

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = TeacherRegisterForm(request.POST)
            if form.is_valid():
                new_form = form.save(commit=False)
                username = form.cleaned_data.get('full_name')
                user_id = User.objects.get(full_name=username).pk
                course = form.cleaned_data.get("course")
                semester = form.cleaned_data.get("semester")
                subject = form.cleaned_data.get('subject')
                user_object = UserTeacher(user_id=user_id)
                user_object.save()
                for c in course:
                    # print(c)
                    course_id = Course.objects.get(name=c).pk
                    # print(course_id)
                    user_object.course.add(course_id)
                for s in semester:
                    semester_id = Semester.objects.get(name=s).pk
                    # print(semester_id)
                    user_object.semester.add(semester_id)
                for sub in subject:
                    subject_id = Subject.objects.get(name=sub).pk
                    subject_obj = Subject.objects.get(name=sub)
                    subject_obj.teacher.add(user_object.id)
                    # print(subject_id)
                    # Subject.objects.create(id=subject_id, teacher_id=user_id)
                new_form.save()
                messages.success(request, f'Account Created. {username} can now login as Teacher.')
                return redirect('users-login')
        else:
            form = TeacherRegisterForm()

        return render(request, self.template_name, {'form': form})


@login_required
def profile(request):
    user = request.user
    if request.method == 'POST':
        if user.is_teacher:
            user_update_form = TeacherUpdateForm(request.POST, instance=request.user)
            profile_update_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
            if user_update_form.is_valid() and profile_update_form.is_valid():
                user_update_form.save()
                profile_update_form.save()
                messages.success(request, f'Your account has been updated!')
                return redirect('users-profile')
        elif user.is_student:
            user_update_form = StudentUpdateForm(request.POST, instance=request.user)
            profile_update_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
            if user_update_form.is_valid() and profile_update_form.is_valid():
                user_update_form.save()
                profile_update_form.save()
                messages.success(request, f'Your account has been updated!')
                return redirect('users-profile')

    else:
        if user.is_teacher:
            user_update_form = TeacherUpdateForm()
            profile_update_form = ProfileUpdateForm()

        elif user.is_student:
            user_update_form = StudentUpdateForm()
            profile_update_form = ProfileUpdateForm()


    context = {
        'user_update_form': user_update_form,
        'profile_update_form': profile_update_form,
    }
    return render(request, 'users/profile.html', context)


@csrf_exempt
def load_subjects(request):
    if request.method == 'POST':
        semester_ids = request.POST.getlist('semester[]')
        course_ids = request.POST.getlist('course[]')

        result = []
        for c_id in course_ids:
            for s_id in semester_ids:
                subjects = Subject.objects.filter(course=c_id).filter(semester_id=s_id)
                for subject in subjects:
                    if not subject.teacher.exists():
                        result.append({'id': subject.id, 'name': subject.name})

        # return JsonResponse(json.dumps(result), safe=False)
        return render(request, "users/dropdown_subject.html", {'subjects': result})
        # return json.dumps(result)
        # return render(request, 'users/dropdown_subject.html', {'subjects': subjects})

